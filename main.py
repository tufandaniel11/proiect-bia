import threading

import pygame
from constants import *
from generator import generate_maze
from solver import solve_maze
import random

from utils import stop_thread

pygame.init()

SCREEN = pygame.display.set_mode(WINDOW)
CLOCK = pygame.time.Clock()
pygame.display.set_caption(TITLE)

BUTTONS = []
SOLVE_THREAD = None


def draw_rect(x, y, len, color):
    pygame.draw.rect(SCREEN, color, [x, y, len, len], 0)


def draw_button(x, y, len, height, text):
    pygame.draw.rect(SCREEN, COLOR_BLACK, [x, y, len, height], 1)
    text_surface = pygame.font.Font(None, 16).render(text, True, COLOR_BLACK)
    text_len = text.__len__() * 6
    SCREEN.blit(text_surface, (x + (len - text_len) / 2, y + 8))


def refresh():
    global MAZE, ENTRANCE, EXIT, current_pos, path_cells, SOLVE_THREAD
    if SOLVE_THREAD is not None and SOLVE_THREAD.is_alive():
        stop_thread(SOLVE_THREAD)
        SOLVE_THREAD = None
    size = random_maze_size()
    MAZE, ENTRANCE, EXIT = generate_maze(size, size)
    current_pos = ENTRANCE
    path_cells = set()


def draw_maze(maze, cur_pos):
    SCREEN.fill(COLOR_WHITE)
    draw_button(2, 2, WIDTH // 2 - 4, HEADER - 4, 'Generate new maze')
    draw_button(WIDTH // 2 + 2, 2, WIDTH // 2 - 4, HEADER - 4, 'Start Auto-Solving')
    if len(BUTTONS) == 0:
        BUTTONS.append({
            'x': 2,
            'y': 2,
            'length': WIDTH // 2 - 4,
            'height': HEADER - 4,
            'click': refresh
        })
        BUTTONS.append({
            'x': WIDTH // 2 + 2,
            'y': 2,
            'length': WIDTH // 2 - 4,
            'height': HEADER - 4,
            'click': start_auto_solving
        })

    size = len(maze)
    cell_size = int(WIDTH / size)
    cell_padding = (WIDTH - (cell_size * size)) / 2
    for y in range(size):
        for x in range(size):
            cell = maze[y][x]
            color = COLOR_BLACK if cell == 1 else COLOR_RED if cell == 3 else COLOR_PATH if cell == 2 else COLOR_WHITE
            if (x, y) == ENTRANCE:
                color = COLOR_GREEN
            elif (x, y) == cur_pos:
                color = COLOR_GREEN if cell == 0 else COLOR_PATH
            elif (x, y) in path_cells:
                color = COLOR_PATH
            draw_rect(cell_padding + x * cell_size, HEADER + cell_padding + y * cell_size, cell_size - 1, color)
    pygame.display.flip()


def start_auto_solving():
    global SOLVE_THREAD
    if SOLVE_THREAD is None or not SOLVE_THREAD.is_alive():
        SOLVE_THREAD = threading.Thread(target=solve_maze, args=(MAZE, ENTRANCE, EXIT, draw_maze))
        SOLVE_THREAD.start()



def dispatcher_click(pos):
    for button in BUTTONS:
        x, y, length, height = button['x'], button['y'], button['length'], button['height']
        pos_x, pos_y = pos
        if x <= pos_x <= x + length and y <= pos_y <= y + height:
            button['click']()


def random_maze_size():
    return random.randint(5, 20) * 2 + 1


if __name__ == '__main__':
    refresh()
    move_direction = None  # Variable to store the current movement direction
    move_interval = 200  # Time interval between each movement (in milliseconds)
    move_timer = pygame.time.get_ticks()  # Timer to track the elapsed time

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit(0)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                dispatcher_click(mouse_pos)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    move_direction = pygame.K_UP
                elif event.key == pygame.K_DOWN:
                    move_direction = pygame.K_DOWN
                elif event.key == pygame.K_LEFT:
                    move_direction = pygame.K_LEFT
                elif event.key == pygame.K_RIGHT:
                    move_direction = pygame.K_RIGHT
                path_cells.add(current_pos if type(current_pos) is not list else None)
            elif event.type == pygame.KEYUP:
                if event.key == move_direction:
                    move_direction = None

        # Handle continuous movement
        if move_direction:
            elapsed_time = pygame.time.get_ticks() - move_timer
            if elapsed_time >= move_interval:
                move_timer = pygame.time.get_ticks()
                if move_direction == pygame.K_UP:
                    if current_pos[1] > 0 and MAZE[current_pos[1] - 1][current_pos[0]] != 1:
                        current_pos = (current_pos[0], current_pos[1] - 1)
                elif move_direction == pygame.K_DOWN:
                    if current_pos[1] < len(MAZE) - 1 and MAZE[current_pos[1] + 1][current_pos[0]] != 1:
                        current_pos = (current_pos[0], current_pos[1] + 1)
                elif move_direction == pygame.K_LEFT:
                    if current_pos[0] > 0 and MAZE[current_pos[1]][current_pos[0] - 1] != 1:
                        current_pos = (current_pos[0] - 1, current_pos[1])
                elif move_direction == pygame.K_RIGHT:
                    if current_pos[0] < len(MAZE[0]) - 1 and MAZE[current_pos[1]][current_pos[0] + 1] != 1:
                        current_pos = (current_pos[0] + 1, current_pos[1])
                path_cells.add(current_pos if type(current_pos) is not list else None)

        draw_maze(MAZE, current_pos)
        CLOCK.tick(10)

